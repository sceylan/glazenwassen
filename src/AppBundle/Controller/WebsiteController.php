<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CityPart;
use AppBundle\Entity\Website;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebsiteController extends Controller
{

    /**
     * Override method to call #containerInitialized method when container set.
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->containerInitialized();
    }

    /**
     * Perform some operations after controller initialized and container set.
     */
    private function containerInitialized()
    {
        $domain = $_SERVER['SERVER_NAME'];
        $domainStripped = str_replace('www.', '', $domain);

        $website = $this->getDoctrine()->getRepository('AppBundle:Website')->findOneBy(array('domain' => $domainStripped));

        // todo validation if website, if not redirect to consturction page.
        $werkgebieden = $this->getDoctrine()->getRepository('AppBundle:CityPart')->findBy(array('website' => $website->getId()), array('sort' => 'ASC'));
        $this->container->get('twig')->addGlobal('website', $website);
        $this->container->get('twig')->addGlobal('werkgebieden', $werkgebieden);

        $url = parse_url($_SERVER['REQUEST_URI']);
        $blogpostpath = str_replace('/', '', $url['path']); ;
        $this->container->get('twig')->addGlobal('blogpostpath', $blogpostpath);
    }

    /**
     * @Route("/", name="home")
     * @Route("/glasbewassing/traditionele-glasbewassing", name="service_traditioneel")
     * @Route("/glasbewassing/telebewassing", name="service_telebewassing")
     * @Route("/glasbewassing/hoogwerker", name="service_hoogwerker")
     * @Route("/glasbewassing/overige-diensten", name="service_overig")
     * @Route("/gevel-reiniging/", name="gevel_service_1")
     * @Route("/graffiti-reiniging/", name="gevel_service_2")
     * @Route("/gevel-impregneren/", name="gevel_service_3")
     * @Route("/voegwerk/", name="gevel_service_4")
     * @Route("/schilderwerk/", name="gevel_service_5")
     * @Route("/overige-diensten/", name="gevel_service_6")
     * @Route("/schoonmaak/", name="schoonmaak_service_1")
     * @Route("/vloeronderhoud/", name="schoonmaak_service_2")
     * @Route("/bouwoplevering/", name="schoonmaak_service_3")
     * @Route("/glasbewassing/", name="schoonmaak_service_4")
     * @Route("/tapijtreiniging/", name="schoonmaak_service_5")
     * @Route("/stukadoren/", name="stukadoor_service_1")
     * @Route("/spachtelputz/", name="stukadoor_service_2")
     * @Route("/sierpleister/", name="stukadoor_service_3")
     * @Route("/latex-spuiten/", name="stukadoor_service_4")
     * @Route("/schilderen/", name="stukadoor_service_5")
     * @Route("/raapwerk/", name="stukadoor_service_6")
     * @Route("/offerte-contact", name="contact")
     * @Template()
     */
    public function pageAction(Request $request)
    {
        $parameters = [];
        $name = $request->get('_route');
        return $this->renderWebsite($name, $parameters);
    }

    /**
     * @Route("/glazenwasserij-in-{cityy}/glazenwasser-{city}-{district}", name="glazen_districts")
     * @Template()
     */
    public function glazenwasserijAction(Request $request, $city, $cityy, $district)
    {
        $parameters = array('city' => $city, 'cityy' => $cityy, 'district' => $district);
        return $this->renderWebsite('district', $parameters);
    }

    /**
     * @Route("/gevelreiniging-in-{cityy}/gevelreiniger-{city}-{district}", name="gevel_districts")
     * @Template()
     */
    public function gevelreinigingAction(Request $request, $city, $cityy, $district)
    {
        $parameters = array('city' => $city, 'cityy' => $cityy, 'district' => $district);
        return $this->renderWebsite('district', $parameters);
    }

    /**
     * @Route("/schoonmaakbedrijven-in-{cityy}/schoonmaakbedrijf-{city}-{district}", name="schoonmaak_districts")
     * @Template()
     */
    public function schoonmaakAction(Request $request, $city, $cityy, $district)
    {
        $parameters = array('city' => $city, 'cityy' => $cityy, 'district' => $district);
        return $this->renderWebsite('district', $parameters);
    }

    /**
     * @param $page
     * @param null $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function renderWebsite($page, $parameters = null)
    {
        if($parameters === null)
        {
            $parameters = array();
        }

        $globals = $this->container->get('twig')->getGlobals();

        /**
         * @var $website Website
         */
        $website = $globals['website'];
        $websiteType = $website->getWebsiteType();

        $allWebsites = $this->getDoctrine()->getRepository('AppBundle:Website')->findBy(['websiteType' => $websiteType]);
        $parameters['allWebsites'] = $allWebsites;

        switch($websiteType)
        {
            case 'glasbewassing':
                switch($page)
                {
                    case 'home':
                        $parameters['pageName'] = 'Home';
                        return $this->render('AppBundle:Glazen:home.html.twig', $parameters);
                    case 'service_traditioneel':
                        $parameters['pageName'] = 'Traditionele Glasbewassing';
                        return $this->render('AppBundle:Glazen:traditioneel.html.twig', $parameters);
                    case 'service_telebewassing':
                        $parameters['pageName'] = 'Telebewassing';
                        return $this->render('AppBundle:Glazen:telebewassing.html.twig', $parameters);
                    case 'service_hoogwerker':
                        $parameters['pageName'] = 'Hoogwerker Glasbewassing';
                        return $this->render('AppBundle:Glazen:hoogwerker.html.twig', $parameters);
                    case 'service_overig':
                        $parameters['pageName'] = 'Overige Diensten';
                        return $this->render('AppBundle:Glazen:overigediensten.html.twig', $parameters);
                    case 'contact':
                        $parameters['pageName'] = 'Offerte - Contact';
                        return $this->render('AppBundle:Glazen:contact.html.twig', $parameters);
                    case 'district':
                        $parameters['pageName'] = 'Glazenwasserij in <br/>';
                        return $this->render('AppBundle:Glazen:district.html.twig', $parameters);
                }
                break;
            case 'gevelreiniging':
                switch($page)
                {
                    case 'home':
                        $parameters['pageName'] = 'Home';
                        return $this->render('AppBundle:Gevel:home.html.twig', $parameters);

                    case 'gevel_service_1':
                        $parameters['pageName'] = 'Gevel Reiniging';
                        return $this->render('AppBundle:Gevel:gevelreiniging.html.twig', $parameters);
                    case 'gevel_service_2':
                        $parameters['pageName'] = 'Graffiti Reiniging';
                        return $this->render('AppBundle:Gevel:graffitireiniging.html.twig', $parameters);
                    case 'gevel_service_3':
                        $parameters['pageName'] = 'Gevel Impregneren';
                        return $this->render('AppBundle:Gevel:gevelimpregneren.html.twig', $parameters);
                    case 'gevel_service_4':
                        $parameters['pageName'] = 'Voegwerk';
                        return $this->render('AppBundle:Gevel:voegwerk.html.twig', $parameters);
                    case 'gevel_service_5':
                        $parameters['pageName'] = 'Schilderwerk';
                        return $this->render('AppBundle:Gevel:schilderwerk.html.twig', $parameters);
                    case 'gevel_service_6':
                        $parameters['pageName'] = 'Overige Diensten';
                        return $this->render('AppBundle:Gevel:overigediensten.html.twig', $parameters);

                    case 'contact':
                        $parameters['pageName'] = 'Offerte - Contact';
                        return $this->render('AppBundle:Gevel:contact.html.twig', $parameters);
                    case 'district':
                        $parameters['pageName'] = 'Gevelreiniging in <br/>';
                        return $this->render('AppBundle:Gevel:district.html.twig', $parameters);
                }
                break;
            case 'schoonmaak':
                switch($page)
                {
                    case 'home':
                        $parameters['pageName'] = 'Home';
                        return $this->render('AppBundle:Schoonmaak:home.html.twig', $parameters);
                    case 'schoonmaak_service_1':
                        $parameters['pageName'] = 'Schoonmaak';
                        return $this->render('AppBundle:Schoonmaak:schoonmaak.html.twig', $parameters);
                    case 'schoonmaak_service_2':
                        $parameters['pageName'] = 'Vloeronderhoud';
                        return $this->render('AppBundle:Schoonmaak:vloeronderhoud.html.twig', $parameters);
                    case 'schoonmaak_service_3':
                        $parameters['pageName'] = 'Bouwoplevering';
                        return $this->render('AppBundle:Schoonmaak:bouwoplevering.html.twig', $parameters);
                    case 'schoonmaak_service_4':
                        $parameters['pageName'] = 'Glasbewassing';
                        return $this->render('AppBundle:Schoonmaak:glasbewassing.html.twig', $parameters);
                    case 'schoonmaak_service_5':
                        $parameters['pageName'] = 'Tapijtreiniging';
                        return $this->render('AppBundle:Schoonmaak:tapijtreiniging.html.twig', $parameters);
                    case 'contact':
                        $parameters['pageName'] = 'Offerte - Contact';
                        return $this->render('AppBundle:Schoonmaak:contact.html.twig', $parameters);
                    case 'district':
                        $parameters['pageName'] = 'Schoonmaakbedrijf in <br/>';
                        return $this->render('AppBundle:Schoonmaak:district.html.twig', $parameters);
                }
                break;
            case 'stukadoor':
                switch($page)
                {
                    case 'home':
                        $parameters['pageName'] = 'Home';
                        return $this->render('AppBundle:Stukadoor:home.html.twig', $parameters);
                    case 'stukadoor_service_1':
                        $parameters['pageName'] = 'Stukadoren';
                        return $this->render('AppBundle:Stukadoor:stukadoren.html.twig', $parameters);
                    case 'stukadoor_service_2':
                        $parameters['pageName'] = 'Spachtelputz';
                        return $this->render('AppBundle:Stukadoor:spachtelputz.html.twig', $parameters);
                    case 'stukadoor_service_3':
                        $parameters['pageName'] = 'Sierpleister';
                        return $this->render('AppBundle:Stukadoor:sierpleister.html.twig', $parameters);
                    case 'stukadoor_service_4':
                        $parameters['pageName'] = 'Latex Spuiten';
                        return $this->render('AppBundle:Stukadoor:latex.html.twig', $parameters);
                    case 'stukadoor_service_5':
                        $parameters['pageName'] = 'Schilderen Binnen en Buiten';
                        return $this->render('AppBundle:Stukadoor:schilderen.html.twig', $parameters);
                    case 'stukadoor_service_6':
                        $parameters['pageName'] = 'Raapwerk';
                        return $this->render('AppBundle:Stukadoor:raapwerk.html.twig', $parameters);
                    case 'contact':
                        $parameters['pageName'] = 'Offerte - Contact';
                        return $this->render('AppBundle:Stukadoor:contact.html.twig', $parameters);
                    case 'district':
                        $parameters['pageName'] = 'Stukadoorbedrijf in <br/>';
                        return $this->render('AppBundle:Stukadoor:district.html.twig', $parameters);
                }
                break;
            default:
                throw new Exception('Undefined websitetype found');
        }

    }

    /**
     * @Route("/blog/", name="blog")
     * @Template()
     */
    public function blogAction(Request $request)
    {

        $globals = $this->container->get('twig')->getGlobals();

        /**
         * @var $website Website
         */
        $website = $globals['website'];
        $websiteType = $website->getWebsiteType();

        $allWebsites = $this->getDoctrine()->getRepository('AppBundle:Website')->findBy(['websiteType' => $websiteType]);
        $parameters['allWebsites'] = $allWebsites;

        $blogs = $this->getDoctrine()->getRepository('AppBundle:Blog')->findBy(['websiteType' => $websiteType]);
        $parameters['blogs'] = $blogs;

        $parameters['pageName'] = 'Blog';

        switch($websiteType)
        {
            case 'glasbewassing':
                return $this->render('AppBundle:Glazen:blog.html.twig', $parameters);
            case 'gevelreiniging':
                return $this->render('AppBundle:Gevel:blog.html.twig', $parameters);
            case 'schoonmaak':
                return $this->render('AppBundle:Schoonmaak:blog.html.twig', $parameters);
            case 'stukadoor':
                return $this->render('AppBundle:Stukadoor:blog.html.twig', $parameters);
            default:
                throw new Exception('Undefined websitetype found');
        }
    }

    /**
     * @Route("/blog/{blogpostpath}/", name="blog_post")
     */
    public function blogPostAction(Request $request, $blogpostpath)
    {

        $blogPost = $this->getDoctrine()->getRepository('AppBundle:Blog')->findOneBy(['path' => $blogpostpath]);

        if(!$blogPost)
            return $this->redirectToRoute('blog');

        $globals = $this->container->get('twig')->getGlobals();

        /**
         * @var $website Website
         */
        $website = $globals['website'];
        $websiteType = $website->getWebsiteType();

        $allWebsites = $this->getDoctrine()->getRepository('AppBundle:Website')->findBy(['websiteType' => $websiteType]);
        $parameters['allWebsites'] = $allWebsites;

        $parameters['pageName'] = $blogPost->getTitle();
        $parameters['blogpost'] = $blogPost;

        switch($websiteType)
        {
            case 'glasbewassing':
                return $this->render('AppBundle:Glazen:blogpost.html.twig', $parameters);
            case 'gevelreiniging':
                return $this->render('AppBundle:Gevel:blogpost.html.twig', $parameters);
            case 'schoonmaak':
                return $this->render('AppBundle:Schoonmaak:blogpost.html.twig', $parameters);
            case 'stukadoor':
                return $this->render('AppBundle:Stukadoor:blogpost.html.twig', $parameters);
            default:
                throw new Exception('Undefined websitetype found');
        }

    }

    /**
     * @Route("/offerte-aanvragen/", name="offerte")
     * @Template()
     */
    public function offerteAction(Request $request)
    {

        $domain = $_SERVER['SERVER_NAME'];
        $domainStripped = str_replace('www.', '', $domain);
        $website = $this->getDoctrine()->getRepository('AppBundle:Website')->findOneBy(array('domain' => $domainStripped));
        $websiteCity = $website->getWebsiteCity();

        $naam = $request->get('naam');
        $email = $request->get('email');
        $telefoonnummer = $request->get('telefoonnummer');
        $bedrijfsnaam = $request->get('bedrijfsnaam');

        $adres = $request->get('adres');
        $huisnummer = $request->get('huisnummer');
        $postcode = $request->get('postcode');
        $stad = $request->get('stad');
        $datum = $request->get('datum');
        $ramen = $request->get('ramen');

        $opmerkingen = $request->get('opmerkingen');

        $recaptchaResponse = $request->get('g-recaptcha-response');


        $recaptchaSecretKeys =
            [
                'localhost' => '6LdyoX0UAAAAANv-Y_lT623M_UWjxJX_95Zi9_yt',
                'glasbewassing' => '6LfNXX0UAAAAAJkTlkdTQahy0ivvCMYYMQwlyIKV',
                'gevelreiniging' => '6LcRl30UAAAAAH2_hmwdUJK8AgZf9n9hqj8W3NcC',
                'schoonmaak' => '6Leqn30UAAAAAIjkkaDD3GlbJ3sF_iwA_-C6Wp-a',
                'stukadoor' => '6Lf3n30UAAAAACtcPmXI8z9AqR_A8SSMg896qFFp',
            ];

        $client = new \GuzzleHttp\Client();
        $captchaRequest = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => $recaptchaSecretKeys[$website->getWebsiteType()],
                'response' => $recaptchaResponse,
                'remoteip' => $this->getUserIP()
            ]
        ]);

        $captchaResult = \GuzzleHttp\json_decode($captchaRequest->getBody()->getContents());

        $websiteType = $website->getWebsiteType();

        switch($websiteType)
        {
            case 'glasbewassing':
                $backgroundColor = '#0082ca';
                break;
            case 'gevelreiniging':
                $backgroundColor = '#c1272d';
                break;
            case 'schoonmaak':
                $backgroundColor = '#007F00';
                break;
            case 'stukadoor':
                $backgroundColor = '#87654F';
                break;
        }

        $allInputValues = array(
            'naam' => $naam,
            'email' => $email,
            'telefoonnummer' => $telefoonnummer,
            'bedrijfsnaam' => $bedrijfsnaam,
            'adres' => $adres,
            'huisnummer' => $huisnummer,
            'postcode' => $postcode,
            'stad' => $stad,
            'datum' => $datum,
            'ramen' => $ramen,
            'opmerkingen' => $opmerkingen,
            'website' => $website,
            'backgroundColor' => $backgroundColor
            );

        $mandatoryInputValues = array($naam, $email, $postcode, $stad);
        $error = false;

        foreach($mandatoryInputValues as $input) {
            if(strlen($input) < 2) {
                $error = true;
            }
        }

        if($captchaResult->success == false)
        {
            $error = true;
        }

        if(!$error) {
            $brancheReceiver = $websiteType. '@dcadienstverlening.nl';

            $message = \Swift_Message::newInstance()
                ->setSubject('Offerte/Contact verzoek ' . $websiteCity)
                ->setFrom(array('server@purevps4.nl' => 'Pure Software'))
                ->setTo(array($brancheReceiver => 'DCA Dienstverlening'))
                ->setCc(array('info@puresoftware.nl' => 'Pure Software'))
                ->setReplyTo(array($email => $naam))
                ->setBody(
                    $this->renderView(
                        '@App/Emails/contact.html.twig',
                        $allInputValues
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);
            $this->addFlash(
                'success',
                'Uw offerte/contact verzoek is succesvol verzonden.'
            );
        } else {
            $this->addFlash(
                'error',
                'Een fout is opgetreden. Verbeter het formulier en probeer het opnieuw. Komt u er niet ui? Neem dan contact op met: 085 - 06 55 477.'
            );
        }

        return $this->redirectToRoute('contact');
    }

    /**
     * @Route("/insertgevel/{password}", name="insertgevel")
     */
    public function insertGevelAction(Request $request, $password) {
        if($password == "sino1905") {

            $cities = $this->getListOfCities();

            $stadsdelen = array(
                'Noord',
                'Oost',
                'Zuid',
                'West',
                'Centrum'
            );

            foreach($cities as $city) {
                $em = $this->getDoctrine()->getManager();

                $formattedCity = str_replace(' ', '', $city);

                $website = new Website();
                $website->setWebsiteType('gevel');
                $website->setDomain('gevelreiniging-in-' . strtolower($formattedCity) . '.nl');
                $website->setWebsiteTitle('De Gevelreinigers van ' . $city);
                $website->setWebsiteCity($city);
                $website->setPhone('085 - 06 55 477');
                $website->setEmail('info@gevelreiniging-in-' . strtolower($formattedCity) . '.nl');
                $website->setWebsite('www.gevelreiniging-in-' . strtolower($formattedCity) . '.nl');
                $website->setSeoTitle('Gevelreiniging in ' . $city);
                $website->setAnalyticsCode(0);
                $em->persist($website);
                $dcount = 0;
                foreach($stadsdelen as $stadsdeel) {
                    $district = new CityPart();
                    $district->setWebsite($website);
                    $district->setName($stadsdeel);
                    $district->setSort($dcount);
                    $dcount++;

                    $em->persist($district);

                }
                $em->flush();
            }

            var_dump('Gevel added!');
            return $this->redirectToRoute('home');

        }
    }

    /**
     * @Route("/insertschoonmaak/{password}", name="insertschoonmaak")
     */
    public function insertSchoonmaakAction(Request $request, $password) {
        if($password == "sino1905") {

            $cities = array(
              'Alkmaar' => 2,
              'Almelo' => 1,
              'Almere' => 1,
              'Alphen aan de Rijn' => 2,
              'Amersfoort' => 1,
              'Amstelveen' => 1,
              'Amsterdam' => 1,
              'Apeldoorn' => 1,
              'Arnhem' => 1,
              'Beesel' => 2,
              'Breda' => 1,
              'Delft' => 1,
              'Den Bosch' => 2,
              'Den Haag' => 1,
              'Den Helder' => 1,
              'Diemen' => 2,
              'Dordrecht' => 2,
              'Ede' => 2,
              'Eindhoven' => 1,
              'Enschede' => 1,
              'Gouda' => 1,
              'Groningen' => 1,
              'Haarlem' => 1,
              'Heerenveen' => 2,
              'Heerhugowaard' => 2,
              'Helmond' => 1,
              'Hengelo' => 2,
              'Hilversum' => 1,
              'Hoofddorp' => 1,
              'Hoorn' => 2,
              'Houten' => 2,
              'Katwijk' => 1,
              'Leeuwarden' => 2,
              'Leiden' => 1,
              'Lelystad' => 1,
              'Nieuwegein' => 2,
              'Nijmegen' => 2,
              'Oss' => 2,
              'Rijswijk' => 2,
              'Roosendaal' => 1,
              'Rotterdam' => 1,
              'Schiedam' => 2,
              'Sneek' => 2,
              'Tilburg' => 1,
              'Utrecht' => 1,
              'Veenendaal' => 2,
              'Vlaardingen' => 2,
              'Westervoort' => 2,
              'Zaandam' => 2,
              'Zevenaar' => 2,
              'Zoetermeer' => 1,
              'Zwolle' => 1,
            );

            $stadsdelen = array(
                'Noord',
                'Oost',
                'Zuid',
                'West',
                'Centrum'
            );

            foreach($cities as $city => $domainType) {
                $em = $this->getDoctrine()->getManager();
                $formattedCity = str_replace(' ', '', $city);

                if($domainType === 1)
                    $domain = 'schoonmaak-bedrijf-';

                if($domainType === 2)
                    $domain = 'schoonmaakbedrijf-';

                $website = new Website();
                $website->setWebsiteType('schoonmaak');
                $website->setDomain($domain . strtolower($formattedCity) . '.nl');
                $website->setWebsiteTitle('De Schoonmakers van ' . $city);
                $website->setWebsiteCity($city);
                $website->setPhone('085 - 06 55 477');
                $website->setEmail("info@$domain" . strtolower($formattedCity) . '.nl');
                $website->setWebsite("www.$domain" . strtolower($formattedCity) . '.nl');
                $website->setSeoTitle('Schoonmakers in ' . $city);
                $website->setAnalyticsCode(0);
                $em->persist($website);
                $dcount = 0;
                foreach($stadsdelen as $stadsdeel) {
                    $district = new CityPart();
                    $district->setWebsite($website);
                    $district->setName($stadsdeel);
                    $district->setSort($dcount);
                    $dcount++;

                    $em->persist($district);

                }
                $em->flush();
            }

            var_dump('Schoonmaak added!');
            return $this->redirectToRoute('home');

        }
    }

    /**
     * @Route("/insertstukadoor/{password}", name="insertstukadoor")
     */
    public function insertStukadoorAction(Request $request, $password) {
        if($password == "sino1905") {

            $cities = array(
                'Alkmaar' => 1,
                'Almelo' => 1,
                'Almere' => 1,
                'Alphen aan de Rijn' => 1,
                'Amersfoort' => 1,
                'Amstelveen' => 1,
                'Amsterdam' => 1,
                'Apeldoorn' => 1,
                'Arnhem' => 1,
                'Beesel' => 1,
                'Breda' => 1,
                'Delft' => 1,
                'Den Bosch' => 1,
                'Den Haag' => 1,
                'Den Helder' => 1,
                'Diemen' => 1,
                'Dordrecht' => 1,
                'Ede' => 1,
                'Eindhoven' => 1,
                'Enschede' => 1,
                'Gouda' => 1,
                'Groningen' => 1,
                'Haarlem' => 1,
                'Heerenveen' => 1,
                'Heerhugowaard' => 1,
                'Helmond' => 1,
                'Hengelo' => 1,
                'Hilversum' => 1,
                'Hoofddorp' => 1,
                'Hoorn' => 1,
                'Houten' => 1,
                'Katwijk' => 1,
                'Leeuwarden' => 1,
                'Leiden' => 1,
                'Lelystad' => 1,
                'Nieuwegein' => 1,
                'Nijmegen' => 1,
                'Oss' => 1,
                'Rijswijk' => 1,
                'Roosendaal' => 1,
                'Rotterdam' => 1,
                'Schiedam' => 1,
                'Sneek' => 1,
                'Tilburg' => 1,
                'Utrecht' => 1,
                'Veenendaal' => 1,
                'Vlaardingen' => 1,
                'Westervoort' => 1,
                'Zaandam' => 1,
                'Zevenaar' => 1,
                'Zoetermeer' => 1,
                'Zwolle' => 1
            );

            $stadsdelen = array(
                'Noord',
                'Oost',
                'Zuid',
                'West',
                'Centrum'
            );

            foreach($cities as $city => $domainType) {
                $em = $this->getDoctrine()->getManager();
                $formattedCity = str_replace(' ', '', $city);

                if($domainType === 1)
                    $domain = 'stukadoor-in-';

//                if($domainType === 2)
//                    $domain = 'schoonmaakbedrijf-';

                $website = new Website();
                $website->setWebsiteType('stukadoor');
                $website->setDomain($domain . strtolower($formattedCity) . '.nl');
                $website->setWebsiteTitle('De Stukadoors van ' . $city);
                $website->setWebsiteCity($city);
                $website->setPhone('085 - 06 55 477');
                $website->setEmail("info@$domain" . strtolower($formattedCity) . '.nl');
                $website->setWebsite("www.$domain" . strtolower($formattedCity) . '.nl');
                $website->setSeoTitle('Stukadoors in ' . $city);
                $website->setAnalyticsCode(0);
                $em->persist($website);
                $dcount = 0;
                foreach($stadsdelen as $stadsdeel) {
                    $district = new CityPart();
                    $district->setWebsite($website);
                    $district->setName($stadsdeel);
                    $district->setSort($dcount);
                    $dcount++;

                    $em->persist($district);

                }
                $em->flush();
            }

            var_dump('Schoonmaak added!');
            return $this->redirectToRoute('home');

        }
    }

    /**
     * Returns visitors IP ADRESS
     * @return mixed
     */
    function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

}

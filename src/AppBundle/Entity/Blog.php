<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table(name="blog")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BlogRepository")
 */
class Blog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="datetime_published", type="integer")
     */
    private $datetimePublished;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="post", type="text")
     */
    private $post;

    /**
     * @var int
     *
     * @ORM\Column(name="website_type", type="string")
     */
    private $websiteType;

    /**
     * @var int
     *
     * @ORM\Column(name="specific_website", type="integer", nullable=true)
     */
    private $specificWebsite;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetimePublished
     *
     * @param integer $datetimePublished
     *
     * @return Blog
     */
    public function setDatetimePublished($datetimePublished)
    {
        $this->datetimePublished = $datetimePublished;

        return $this;
    }

    /**
     * Get datetimePublished
     *
     * @return int
     */
    public function getDatetimePublished()
    {
        return $this->datetimePublished;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Blog
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return Blog
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set specificWebsite
     *
     * @param integer $specificWebsite
     *
     * @return Blog
     */
    public function setSpecificWebsite($specificWebsite)
    {
        $this->specificWebsite = $specificWebsite;

        return $this;
    }

    /**
     * Get specificWebsite
     *
     * @return int
     */
    public function getSpecificWebsite()
    {
        return $this->specificWebsite;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Blog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Blog
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CityPart
 *
 * @ORM\Table(name="city_part")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityPartRepository")
 */
class CityPart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="website_id", type="integer", unique=false)
     */
    private $websiteId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer")
     */
    private $sort;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Website", inversedBy="cityParts")
     */
    private $website;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set websiteId
     *
     * @param integer $websiteId
     * @return CityPart
     */
    public function setWebsiteId($websiteId)
    {
        $this->websiteId = $websiteId;

        return $this;
    }

    /**
     * Get websiteId
     *
     * @return integer 
     */
    public function getWebsiteId()
    {
        return $this->websiteId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CityPart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cityParts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cityParts
     *
     * @param \AppBundle\Entity\Website $cityParts
     * @return CityPart
     */
    public function addCityPart(\AppBundle\Entity\Website $cityParts)
    {
        $this->cityParts[] = $cityParts;

        return $this;
    }

    /**
     * Remove cityParts
     *
     * @param \AppBundle\Entity\Website $cityParts
     */
    public function removeCityPart(\AppBundle\Entity\Website $cityParts)
    {
        $this->cityParts->removeElement($cityParts);
    }

    /**
     * Get cityParts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCityParts()
    {
        return $this->cityParts;
    }

    /**
     * Set website
     *
     * @param \AppBundle\Entity\Website $website
     * @return CityPart
     */
    public function setWebsite(\AppBundle\Entity\Website $website = null)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return \AppBundle\Entity\Website 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return CityPart
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer 
     */
    public function getSort()
    {
        return $this->sort;
    }
}

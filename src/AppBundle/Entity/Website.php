<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Website
 *
 * @ORM\Table(name="website")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WebsiteRepository")
 */
class Website
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, unique=true)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $websiteType;

    /**
     * @var string
     *
     * @ORM\Column(name="website_title", type="string", length=255)
     */
    private $websiteTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="website_city", type="string", length=255)
     */
    private $websiteCity;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=255)
     */
    private $seoTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="analytics_code", type="integer")
     */
    private $analyticsCode;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CityPart", mappedBy="website")
     */
    private $cityParts;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Website
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set websiteTitle
     *
     * @param string $websiteTitle
     * @return Website
     */
    public function setWebsiteTitle($websiteTitle)
    {
        $this->websiteTitle = $websiteTitle;

        return $this;
    }

    /**
     * Get websiteTitle
     *
     * @return string 
     */
    public function getWebsiteTitle()
    {
        return $this->websiteTitle;
    }

    /**
     * Set websiteDescription
     *
     * @param string $websiteDescription
     * @return Website
     */
    public function setWebsiteDescription($websiteDescription)
    {
        $this->websiteDescription = $websiteDescription;

        return $this;
    }

    /**
     * Get websiteDescription
     *
     * @return string 
     */
    public function getWebsiteDescription()
    {
        return $this->websiteDescription;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Website
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Website
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Website
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return Website
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return Website
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set websiteSlogan
     *
     * @param string $websiteSlogan
     * @return Website
     */
    public function setWebsiteSlogan($websiteSlogan)
    {
        $this->websiteSlogan = $websiteSlogan;

        return $this;
    }

    /**
     * Get websiteSlogan
     *
     * @return string 
     */
    public function getWebsiteSlogan()
    {
        return $this->websiteSlogan;
    }

    /**
     * Set websiteCity
     *
     * @param string $websiteCity
     * @return Website
     */
    public function setWebsiteCity($websiteCity)
    {
        $this->websiteCity = $websiteCity;

        return $this;
    }

    /**
     * Get websiteCity
     *
     * @return string 
     */
    public function getWebsiteCity()
    {
        return $this->websiteCity;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cityParts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cityParts
     *
     * @param \AppBundle\Entity\CityPart $cityParts
     * @return Website
     */
    public function addCityPart(\AppBundle\Entity\CityPart $cityParts)
    {
        $this->cityParts[] = $cityParts;

        return $this;
    }

    /**
     * Remove cityParts
     *
     * @param \AppBundle\Entity\CityPart $cityParts
     */
    public function removeCityPart(\AppBundle\Entity\CityPart $cityParts)
    {
        $this->cityParts->removeElement($cityParts);
    }

    /**
     * Get cityParts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCityParts()
    {
        return $this->cityParts;
    }

    /**
     * Set analyticsCode
     *
     * @param integer $analyticsCode
     *
     * @return Website
     */
    public function setAnalyticsCode($analyticsCode)
    {
        $this->analyticsCode = $analyticsCode;

        return $this;
    }

    /**
     * Get analyticsCode
     *
     * @return integer
     */
    public function getAnalyticsCode()
    {
        return $this->analyticsCode;
    }

    /**
     * Set websiteType
     *
     * @param string $websiteType
     *
     * @return Website
     */
    public function setWebsiteType($websiteType)
    {
        $this->websiteType = $websiteType;

        return $this;
    }

    /**
     * Get websiteType
     *
     * @return string
     */
    public function getWebsiteType()
    {
        return $this->websiteType;
    }
}

$(document).ready(function(){
    $("#contactform").validate({
        rules:
        {
            required:
            {
                required: true
            },
            email:
            {
                required: true,
                email: true
            },
            url:
            {
                required: true,
                url: true
            },
            date:
            {
                required: true,
                date: true
            },
            min:
            {
                required: true,
                minlength: 5
            },
            max:
            {
                required: true,
                maxlength: 5
            },
            range:
            {
                required: true,
                rangelength: [5, 10]
            },
            digits:
            {
                required: true,
                digits: true
            },
            number:
            {
                required: true,
                number: true
            },
            minVal:
            {
                required: true,
                min: 5
            },
            maxVal:
            {
                required: true,
                max: 100
            },
            rangeVal:
            {
                required: true,
                range: [5, 100]
            }
        },

        // Messages for form validation
        messages: {},
        highlight: function (element) {
            $(element).addClass('error')
        },
        unhighlight: function (element) {
            $(element).removeClass('error')
        },
        errorPlacement: function(error, element)
        {
            return false;
            // error.insertAfter(element.parent());
        }


    });
});